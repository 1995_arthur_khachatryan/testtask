package com.khachatryan.arthur.a1995.testtask;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.google.gson.Gson;
import com.khachatryan.arthur.a1995.testtask.pojos.RequestConfig;

import java.io.File;
import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity implements TestMethodCalledListener {

    private static final String CONFIG_URL = "http://andr.appforsearch.com/config.php";

    private static final int WRITE_EXTERNAL_STORAGE_REQUEST = 100;

    private TestWebView mWebView;

    private String mTestFunctionParams;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser);

        mWebView = (TestWebView) findViewById(R.id.activity_browser_web_view);
    }

    @Override
    protected void onStart() {
        super.onStart();

        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder().url(CONFIG_URL).build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                Toast.makeText(MainActivity.this, "Can't load config file", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (response.body() == null) return;
                RequestConfig requestConfig = new Gson().fromJson(response.body().charStream(), RequestConfig.class);
                runOnUiThread(() -> mWebView.loadUrl(requestConfig, MainActivity.this));
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        mTestFunctionParams = null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case WRITE_EXTERNAL_STORAGE_REQUEST:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    writeTestResult();
                } else {
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onTestMethodCalled(String args) {
        mTestFunctionParams = args;
        checkForWritePermission();
    }

    private void checkForWritePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    WRITE_EXTERNAL_STORAGE_REQUEST);
        } else {
            writeTestResult();
        }
    }

    private void writeTestResult() {
        if (mTestFunctionParams == null) {
            Toast.makeText(MainActivity.this, "There is no function params", Toast.LENGTH_SHORT).show();
            return;
        }
        File file = new File(Environment.getExternalStorageDirectory(), "TestTask.txt");
        new FileWriter().write(file, mTestFunctionParams, new FileWriter.Callback() {
            @Override
            public void onSuccess(String filePath) {
                Toast.makeText(
                        MainActivity.this,
                        "File successfully written.\n" + filePath,
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFail(IOException e) {
                Toast.makeText(MainActivity.this, "File writing failed.", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
