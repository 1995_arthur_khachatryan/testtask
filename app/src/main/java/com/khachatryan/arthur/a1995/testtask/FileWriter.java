package com.khachatryan.arthur.a1995.testtask;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

class FileWriter {

    interface Callback {
        void onSuccess(String filePath);

        void onFail(IOException e);
    }

    void write(File file, String text, Callback callback) {
        try {

            FileOutputStream fos = new FileOutputStream(file);
            fos.write(text.getBytes());
            fos.close();
            if (callback != null) callback.onSuccess(file.getPath());

        } catch (IOException e) {
            if (callback != null) callback.onFail(e);
        }
    }
}
