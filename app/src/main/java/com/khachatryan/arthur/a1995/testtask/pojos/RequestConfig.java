package com.khachatryan.arthur.a1995.testtask.pojos;

import com.google.gson.annotations.SerializedName;

public class RequestConfig {

    @SerializedName("url")
    private String mUrl;
    @SerializedName("method")
    private String mMethod;
    @SerializedName("proxy")
    private ProxyParams mProxy;

    public String getUrl() {
        return mUrl;
    }

    public String getMethod() {
        return mMethod;
    }

    public ProxyParams getProxy() {
        return mProxy;
    }
}
