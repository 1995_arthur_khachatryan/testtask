package com.khachatryan.arthur.a1995.testtask;

import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.khachatryan.arthur.a1995.testtask.pojos.ProxyParams;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Authenticator;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

class TestWebViewClient extends WebViewClient {

    private ProxyParams mProxyParams;
    private String mJsInterfaceName;
    private String mTestMethodName;

    public TestWebViewClient() {

    }

    public void setProxyParams(ProxyParams mProxyParams) {
        this.mProxyParams = mProxyParams;
    }

    public void setJsInterfaceName(String jsInterfaceName) {
        this.mJsInterfaceName = jsInterfaceName;
    }

    public void setTestMethodName(String testMethodName) {
        this.mTestMethodName = testMethodName;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        String jsTemp = "javascript: " +
                "function " + mTestMethodName + "(args){" +
                mJsInterfaceName + ".testMethodCall(args);" +
                "}";
        view.loadUrl(jsTemp);
    }

    @Override
    public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
        if (mProxyParams == null) return super.shouldInterceptRequest(view, request);

        // Make proxy request with custom mOkHttpClient
        Request okHttpRequest = new Request.Builder()
                .url(request.getUrl().toString())
                .build();
        Response okHttpResponse;
        try {
            okHttpResponse = buildProxyHttpClient(mProxyParams).newCall(okHttpRequest).execute();
        } catch (IOException e) {
            e.printStackTrace();
            return super.shouldInterceptRequest(view, request);
        }

        // Obtaining header name-value map:
        Map<String, String> okHttpResponseHeaderMap = new HashMap<>();
        for (String name : okHttpResponse.headers().names()) {
            okHttpResponseHeaderMap.put(name, okHttpResponse.headers().get(name));
        }

        // Obtaining mime-type from content-type:
        String okHttpResponseContentType = okHttpResponse.header("Content-Type");
        if (okHttpResponseContentType == null)
            return super.shouldInterceptRequest(view, request);
        String okHttpResponseMimeType = okHttpResponseContentType.contains(";") ?
                okHttpResponseContentType.substring(0, okHttpResponseContentType.indexOf(";"))
                : okHttpResponseContentType;

        // Returning response in proper format:
        return new WebResourceResponse(
                okHttpResponseMimeType,
                okHttpResponse.header("Content-Encoding"),
                okHttpResponse.code(),
                okHttpResponse.message(),
                okHttpResponseHeaderMap,
                okHttpResponse.body().byteStream()

        );
    }

    private OkHttpClient buildProxyHttpClient(ProxyParams proxyParams) {
        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();

        Proxy proxy = new Proxy(
                Proxy.Type.HTTP,
                new InetSocketAddress(proxyParams.getIp(), Integer.parseInt(proxyParams.getPort()))
        );

        Authenticator proxyAuthenticator = (route, response) -> {
            String credential = Credentials.basic(proxyParams.getUsername(), proxyParams.getPassword());
            return response.request().newBuilder()
                    .header("Proxy-Authorization", credential)
                    .build();
        };

        okHttpClientBuilder = okHttpClientBuilder.proxy(proxy).proxyAuthenticator(proxyAuthenticator);

        return okHttpClientBuilder.build();
    }
}
