package com.khachatryan.arthur.a1995.testtask;

interface TestMethodCalledListener {
    void onTestMethodCalled(String args);
}
