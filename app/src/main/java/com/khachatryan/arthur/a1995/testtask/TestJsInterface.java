package com.khachatryan.arthur.a1995.testtask;

import android.webkit.JavascriptInterface;

class TestJsInterface {

    private TestMethodCalledListener mTestMethodCalledListener;

    void setTestMethodCalledListener(TestMethodCalledListener mTestMethodCalledListener) {
        this.mTestMethodCalledListener = mTestMethodCalledListener;
    }

    @JavascriptInterface
    public void testMethodCall(String args) {
        if (mTestMethodCalledListener != null) mTestMethodCalledListener.onTestMethodCalled(args);
    }
}
