package com.khachatryan.arthur.a1995.testtask.pojos;

import com.google.gson.annotations.SerializedName;

public class ProxyParams {

    @SerializedName("ip")
    private String mIp;
    @SerializedName("port")
    private String mPort;
    @SerializedName("username")
    private String mUsername;
    @SerializedName("password")
    private String mPassword;

    public String getIp() {
        return mIp;
    }

    public String getPort() {
        return mPort;
    }

    public String getUsername() {
        return mUsername;
    }

    public String getPassword() {
        return mPassword;
    }
}
