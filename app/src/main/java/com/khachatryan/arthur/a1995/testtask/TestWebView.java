package com.khachatryan.arthur.a1995.testtask;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;

import com.khachatryan.arthur.a1995.testtask.pojos.RequestConfig;

public class TestWebView extends WebView {

    private static final String TEST_JS_INTERFACE_NAME = "Android";

    private TestWebViewClient mTestWebViewClient = new TestWebViewClient();
    private TestJsInterface mTestJsInterface = new TestJsInterface();

    public TestWebView(Context context) {
        super(context);
        init();
    }

    public TestWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TestWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void loadUrl(RequestConfig requestConfig, TestMethodCalledListener testMethodCalledListener) {
        mTestWebViewClient.setProxyParams(requestConfig.getProxy());
        mTestWebViewClient.setJsInterfaceName(TEST_JS_INTERFACE_NAME);
        mTestWebViewClient.setTestMethodName(requestConfig.getMethod());
        mTestJsInterface.setTestMethodCalledListener(testMethodCalledListener);
        loadUrl(requestConfig.getUrl());
    }

    private void init() {
        getSettings().setJavaScriptEnabled(true);
        setWebViewClient(mTestWebViewClient);
        addJavascriptInterface(mTestJsInterface, TEST_JS_INTERFACE_NAME);
    }
}
